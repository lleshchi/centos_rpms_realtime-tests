#!/usr/bin/bash

# make sure we have rt-tests installed
if rpm -q --quiet realtime-tests; then
    :
else
    sudo dnf install -y realtime-tests
    if [[ $? != 0 ]]; then
       echo "install of realtime-tests failed!"
       exit 1
    fi
fi

progs="/usr/bin/cyclicdeadline
/usr/bin/cyclictest
/usr/bin/deadline_test
/usr/bin/determine_maximum_mpps.sh
/usr/bin/get_cyclictest_snapshot
/usr/bin/hackbench
/usr/bin/hwlatdetect
/usr/bin/oslat
/usr/bin/pi_stress
/usr/bin/pip_stress
/usr/bin/pmqtest
/usr/bin/ptsematest
/usr/bin/queuelat
/usr/bin/rt-migrate-test
/usr/bin/signaltest
/usr/bin/sigwaittest
/usr/bin/ssdd
/usr/bin/svsematest"

# check bins
for prog in ${progs}; do
    if [[ ! -f $prog ]]; then
       echo "$prog: not found"
       exit 2
    fi
    if [[ ! -e $prog ]]; then
       echo "$prog: not executable"
       exit 3
    fi
done

exit 0
